// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE fil7e.

struct Config {
    lines_count: u32,
    segments_count: u32,
}

struct Line {
    a: f32,
    b: f32,
    c: f32,
    d: f32,
    x0: f32,
    y0: f32,
    dx: f32,
    dy: f32,
    order: u32,
}

@group(0) @binding(0) var<uniform> config: Config;
@group(0) @binding(1) var<storage> points: array<vec2<f32>>;
@group(0) @binding(2) var<storage> orders: array<u32>;
@group(0) @binding(3) var<storage, write> lines: array<Line>;

// As per gpu/painter/src/lib.rs.
let PIXEL_WIDTH: f32 = 16.0;
let NONE: u32 = 0xffffffffu;

fn prepareLine(p0: vec2<f32>, p1: vec2<f32>, order: u32) -> Line {
    if order == NONE {
        return Line(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0u);
    }

    let dp = p1 - p0;
    let dp_recip = 1.0 / dp;

    let t_offset = select(
        vec2(0.0, 0.0),
        max((ceil(p0) - p0) * dp_recip, -fract(p0) * dp_recip),
        dp != vec2<f32>(0.0, 0.0));


    let a = abs(dp_recip.x);
    let b = abs(dp_recip.y);
    let c = t_offset.x;
    let d = t_offset.y;

    // Converting to sub-pixel by multiplying with `PIXEL_WIDTH`.
    let sp0 = p0 * PIXEL_WIDTH;
    let sdp = dp * PIXEL_WIDTH;
    return Line(a, b, c, d, sp0.x, sp0.y, sdp.x, sdp.y, order);
}


@compute @workgroup_size(128)
fn prepareLines(
    @builtin(global_invocation_id) global_id_vec: vec3<u32>
) {
    let workgroup_size = 128u;
    let max_invocation_count = 16u;

    for (
        var line_id = global_id_vec.x;
        line_id < config.lines_count;
        line_id += workgroup_size * max_invocation_count) {

        let p0 = points[line_id];
        let p1 = points[line_id + 1u];
        let order = orders[line_id];

        lines[line_id] = prepareLine(p0, p1, order);
    }

}
