// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion, Throughput};
use forma::GeomId;
use surpass::{
    rasterizer::Rasterizer, Layer, LinesBuilder, Order, PathBuilder, Point, TILE_HEIGHT, TILE_WIDTH,
};

pub fn build_lines(c: &mut Criterion) {
    let mut group = c.benchmark_group("build_lines");
    for lines in [100, 1000, 10000] {
        let mut builder = PathBuilder::new();

        for line in 0..lines {
            builder.line_to(Point::new(line as f32 * 10.0, if line % 2 == 0 { 10.0 } else { 1.0 }));
        }

        let path = builder.build();

        let mut builder = Some(LinesBuilder::new());

        builder.as_mut().unwrap().push_path(GeomId::default(), &path);

        let len = builder.as_ref().unwrap().len();

        group.throughput(Throughput::Elements(len as u64));
        group.bench_with_input(BenchmarkId::from_parameter(len), &len, |b, _| {
            b.iter(|| {
                let lines = builder.take().unwrap().build(|_| None);
                builder = Some(lines.unwrap());
            });
        });
    }
    group.finish();
}

pub fn rasterize(c: &mut Criterion) {
    let mut group = c.benchmark_group("rasterize");
    for lines in [100, 1000, 10000] {
        let mut builder = PathBuilder::new();

        for line in 0..lines {
            builder.line_to(Point::new(line as f32 * 10.0, if line % 2 == 0 { 10.0 } else { 1.0 }));
        }

        let path = builder.build();

        let mut builder = LinesBuilder::new();

        builder.push_path(GeomId::default(), &path);

        let lines = builder.build(|_| {
            Some(Layer {
                is_enabled: true,
                order: Some(Order::new(0).unwrap()),
                affine_transform: None,
            })
        });

        let mut rasterizer: Rasterizer<TILE_WIDTH, TILE_HEIGHT> = Rasterizer::new();

        rasterizer.rasterize(&lines);

        let len = rasterizer.segments().len();

        group.throughput(Throughput::Elements(len as u64));
        group.bench_with_input(BenchmarkId::from_parameter(len), &len, |b, _| {
            b.iter(|| {
                rasterizer.rasterize(&lines);
            });
        });
    }
    group.finish();
}

pub fn rasterize_sort(c: &mut Criterion) {
    let mut group = c.benchmark_group("rasterize_sort");
    for lines in [100, 1000, 10000] {
        let mut builder = PathBuilder::new();

        for line in 0..lines {
            builder.line_to(Point::new(line as f32 * 10.0, if line % 2 == 0 { 10.0 } else { 1.0 }));
        }

        let path = builder.build();

        let mut builder = LinesBuilder::new();

        builder.push_path(GeomId::default(), &path);

        let lines = builder.build(|_| {
            Some(Layer {
                is_enabled: true,
                order: Some(Order::new(0).unwrap()),
                affine_transform: None,
            })
        });

        let mut rasterizer: Rasterizer<TILE_WIDTH, TILE_HEIGHT> = Rasterizer::new();

        rasterizer.rasterize(&lines);

        let len = rasterizer.segments().len();

        group.throughput(Throughput::Elements(len as u64));
        group.bench_with_input(BenchmarkId::from_parameter(len), &len, |b, _| {
            b.iter(|| {
                rasterizer.rasterize(&lines);
                rasterizer.sort();
            });
        });
    }
    group.finish();
}

criterion_group!(benches, build_lines, rasterize, rasterize_sort);
criterion_main!(benches);
