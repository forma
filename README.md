# forma. The parallel CPU vector renderer


*forma* is a highly efficient vector content renderer that makes use of all
processor cores and vector units.

## Getting started

Requirements:

* git
* [rust](https://rustup.rs)

Clone the forma repositories from Fuchsia by running:

```sh
./clone.sh
```

Build the demo:

```sh
cargo build --release
```

Then run:

```sh
./target/release/demo rive assets/rive/juice.riv
```
