// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import init, { initThreadPool, context_new_rive, context_draw } from './pkg/web.js';

let context;
let rive = 'juice.riv';
let width = 1000;
let height = 1000;

function riveBuffer() {
  const req = new XMLHttpRequest();
  req.open("GET", "/assets/rive/" + rive, false);
  req.responseType = "arraybuffer";

  req.send(null);

  return new Uint8Array(req.response);
}

onmessage = function(message) {
  width = message.data.width;
  height = message.data.height;

  if (rive !== message.data.rive) {
    rive = message.data.rive

    context = context_new_rive(width, height, riveBuffer());
  }

  if (message.data.elapsed) {
    postMessage(context_draw(context, width, height, message.data.elapsed, message.data.partials));
  }
}

async function initialize() {
  await init();
  await initThreadPool(navigator.hardwareConcurrency);

  context = context_new_rive(width, height, riveBuffer());

  postMessage(context_draw(context, width, height, 0, false));
}

initialize();
