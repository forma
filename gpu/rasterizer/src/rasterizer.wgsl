// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

struct Config {
    lines_count: u32,
    segments_count: u32,
    segments_padded_count: u32,
}

struct PixelSegment {
    lo: u32,
    hi: u32,
}

struct Line {
    a: f32,
    b: f32,
    c: f32,
    d: f32,
    x0: f32,
    y0: f32,
    dx: f32,
    dy: f32,
    order: u32,
}

@group(0) @binding(0) var<uniform> config: Config;
@group(0) @binding(1) var<storage> lines: array<Line>;
@group(0) @binding(2) var<storage> lines_lengths: array<u32>;
@group(0) @binding(3) var<storage, write> pixel_segments: array<PixelSegment>;

// Binary search.
fn findLineId(segment_id: u32, lines_count: u32) -> u32 {
    // Let line_id be min(i) ∀ i ∈ [0; segments_len) where lines_orders[i] > segment_id.
    // Binary search in range [lo ; hi].
    var lo: u32 = 0u;
    var hi: u32 = lines_count;
    for (var i = lines_count; i > 0u ; i >>= 1u){
        let mid: u32 = lo + ((hi - lo) >> 1u);
        let is_greater = lines_lengths[mid] > segment_id;
        lo = select(mid + 1u, lo, is_greater);
        hi = select(hi, mid, is_greater);
    }
    return lo;
}

// Returns true when the value equals +Inf or -Inf.
fn isInf(v: f32) -> bool {
    let clear_sign_mask = 0x7fffffffu;
    let infinity = 0x7f800000u;
    return (bitcast<u32>(v) & clear_sign_mask) == infinity;
}

// Rounds to closest integer or towards -Inf if there is a draw.
fn roundToI32(v: f32) -> i32 {
    return i32(floor(v + 0.5));
}

let TILE_WIDTH_SHIFT = 4u;
let TILE_HEIGHT_SHIFT = 2u;
let MAX_WIDTH_SHIFT = 16u;
let MAX_HEIGHT_SHIFT = 15u;
let TILE_BIAS = 1;
let LAYER_ID_BIT_SIZE = 21u;
let DOUBLE_AREA_MULTIPLIER_BIT_SIZE = 6u;
let COVER_BIT_SIZE = 6u;

fn packPixelSegment(
    layer_id: u32,
    tile_x: i32,
    tile_y: i32,
    local_x: u32,
    local_y: u32,
    double_area_multiplier: u32,
    cover: i32,
    ) -> PixelSegment {

    var ps: PixelSegment;

    ps.hi = u32(max(0, tile_y + TILE_BIAS)) << (
        32u - (MAX_HEIGHT_SHIFT - TILE_HEIGHT_SHIFT));

    ps.hi = insertBits(
        ps.hi,
        u32(max(0, tile_x + TILE_BIAS)),
        32u - (MAX_WIDTH_SHIFT - TILE_WIDTH_SHIFT) -
            (MAX_HEIGHT_SHIFT - TILE_HEIGHT_SHIFT),
        MAX_WIDTH_SHIFT - TILE_WIDTH_SHIFT,
    );

    ps.hi = insertBits(
        ps.hi,
        layer_id >> (32u - TILE_WIDTH_SHIFT - TILE_HEIGHT_SHIFT -
            DOUBLE_AREA_MULTIPLIER_BIT_SIZE - COVER_BIT_SIZE),
        0u,
        32u - (MAX_WIDTH_SHIFT - TILE_WIDTH_SHIFT) -
            (MAX_HEIGHT_SHIFT - TILE_HEIGHT_SHIFT),
    );

    ps.lo = layer_id << (
        TILE_WIDTH_SHIFT + TILE_HEIGHT_SHIFT +
        DOUBLE_AREA_MULTIPLIER_BIT_SIZE + COVER_BIT_SIZE);

    ps.lo =  insertBits(
        ps.lo,
        local_x,
        TILE_HEIGHT_SHIFT + DOUBLE_AREA_MULTIPLIER_BIT_SIZE + COVER_BIT_SIZE,
        TILE_WIDTH_SHIFT,
    );

    ps.lo = insertBits(
        ps.lo,
        local_y,
        DOUBLE_AREA_MULTIPLIER_BIT_SIZE + COVER_BIT_SIZE,
        TILE_HEIGHT_SHIFT,
    );

    ps.lo = insertBits(
        ps.lo,
        double_area_multiplier,
        COVER_BIT_SIZE,
        DOUBLE_AREA_MULTIPLIER_BIT_SIZE,
    );

    ps.lo = insertBits(
        ps.lo,
        u32(cover),
        0u,
        COVER_BIT_SIZE,
    );

    return ps;
}


// This finds the ith term in the ordered union of two sequences:
//
// 1. a * x + c
// 2. b * x + d
//
// It works by estimating the amount of items that came from sequence 1 and
// sequence 2 such that the next item will be the ith. This results in two
// indices from each sequence. The final step is to simply pick the smaller one
// which naturally comes next.
fn find(i: i32, sum_recip: f32, cd: f32, a: f32, b: f32, c: f32, d: f32) -> f32 {
    let BIAS: f32 = -0.0000005;

    let i = f32(i);

    let ja = select(ceil((b * i - cd) * sum_recip + BIAS), i, isInf(b));
    let jb = select(ceil((a * i + cd) * sum_recip + BIAS), i, isInf(a));

    let guess_a = a * ja + c;
    let guess_b = b * jb + d;

    return min(guess_a, guess_b);
}

// As per gpu/painter/src/lib.rs.
let TW: i32 = 16;
let TW_SHIFT: u32 = 4u;
let TH: i32 = 4;
let TH_SHIFT: u32 = 2u;
let PIXEL_WIDTH: i32 = 16;
let PIXEL_SHIFT: u32 = 4u;

fn computePixelSegment(li: u32, pi: u32) -> PixelSegment {
    let line_ = lines[li];
    let a = line_.a;
    let b = line_.b;
    let c = line_.c;
    let d = line_.d;

    let i: i32 = i32(pi) - i32(c != 0.0) - i32(d != 0.0);

    let i0 = i;
    let i1 = i + 1;

    let sum_recip = 1.0 / (a + b);
    let cd = c - d;

    let t0 = max(find(i0, sum_recip, cd, a, b, c, d), 0.0);
    let t1 = min(find(i1, sum_recip, cd, a, b, c, d), 1.0);

    let x0f = t0 * line_.dx + line_.x0;
    let y0f = t0 * line_.dy + line_.y0;
    let x1f = t1 * line_.dx + line_.x0;
    let y1f = t1 * line_.dy + line_.y0;

    let x0_sub: i32 = roundToI32(x0f);
    let x1_sub: i32 = roundToI32(x1f);
    let y0_sub: i32 = roundToI32(y0f);
    let y1_sub: i32 = roundToI32(y1f);

    let border_x: i32 = min(x0_sub, x1_sub) >> PIXEL_SHIFT;
    let border_y: i32 = min(y0_sub, y1_sub) >> PIXEL_SHIFT;

    let tile_x: i32 = (border_x >> TW_SHIFT);
    let tile_y: i32 = (border_y >> TH_SHIFT);
    let local_x: u32 = u32(border_x & (TW - 1)); //? IS MASK USEFUL?
    let local_y: u32 = u32(border_y & (TH - 1));

    let border = (border_x << PIXEL_SHIFT) + PIXEL_WIDTH;
    let height = y1_sub - y0_sub;

    let double_area_multiplier: u32 =
        u32(abs(x1_sub - x0_sub) + 2 * (border - max(x0_sub, x1_sub)));
    let cover: i32 = height;

    return packPixelSegment(
        line_.order,
        tile_x,
        tile_y,
        local_x,
        local_y,
        double_area_multiplier,
        cover,
    );
}

@compute @workgroup_size(128)
fn rasterize(
    @builtin(global_invocation_id) global_id_vec: vec3<u32>
) {
    let workgroup_size = 128u;
    let max_invocation_count = 16u;
    for (
        var segment_id = global_id_vec.x;
        segment_id < config.segments_padded_count;
        segment_id += workgroup_size * max_invocation_count) {

        let li = findLineId(segment_id, config.lines_count);
        let pi = segment_id - select(0u, lines_lengths[max(0u, li - 1u)], li > 0u);
        pixel_segments[segment_id] = computePixelSegment(li, pi);

        // Set segment beyond the last line with padding to u64 maximal value,
        // so that it stays at the end of the buffer after sort, and the painter
        // can ignore them based on the exact segment count.
        if segment_id >= config.segments_count {
            pixel_segments[segment_id] = PixelSegment(0xffffffffu, 0xffffffffu);
        }
    }
}