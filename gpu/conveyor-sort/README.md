# (WIP) conveyor-sort GPU integer sorter

conveyor-sort is a WebGPU-backed 64-bit integer based on the merge sort
algorithm impemented [Modern GPU] by Sean Baxter. It has the following focus
areas, in this order:

* portability
* understandability
* performance

Current compatibility:

- [x] AMD GCN
- [x] Arm Bifrost
- [x] Intel Gen10
- [x] Nvidia Turing

**Note:** The shader is early in development since features of WGSL are still
in flux and not all of them supported yet in wgpu. For example, some functions
could be simplfied once pointers become more flexible.

[Modern GPU]: https://moderngpu.github.io/mergesort.html

## Preliminary benchmarks (results in million elements per second)

### AMD Ryzen 4700u, 7 CUs, 3200MHz DDR4

#### Uniform random

| u64 array size | parallel pdqsort (CPU) | conveyor-sort (GPU) |
|---------------:|-----------------------:|--------------------:|
|           1024 |                  43.39 |                 N/A |
|           4096 |                   8.13 |                 N/A |
|          65536 |                 238.23 |                 N/A |
|        1048576 |                 253.39 |                 N/A |
|       16777216 |                 171.22 |                 N/A |

#### Pre-sorted chunks

| u64 array size | parallel pdqsort (CPU) | conveyor-sort (GPU) |
|---------------:|-----------------------:|--------------------:|
|           1024 |                 152.84 |                 N/A |
|           4096 |                  79.69 |                 N/A |
|          65536 |                 203.97 |                 N/A |
|        1048576 |                 239.24 |                 N/A |
|       16777216 |                 154.24 |                 N/A |

### Samsung Exynos 9810, 18 CUs, 1800MHz LPDDR4X

#### Uniform random

| u64 array size | parallel pdqsort (CPU) | conveyor-sort (GPU) |
|---------------:|-----------------------:|--------------------:|
|           1024 |                  17.19 |               11.99 |
|           4096 |                   1.30 |               31.68 |
|          65536 |                   3.02 |               75.11 |
|        1048576 |                   9.37 |               76.96 |
|       16777216 |                  37.79 |               64.24 |

#### Pre-sorted chunks

| u64 array size | parallel pdqsort (CPU) | conveyor-sort (GPU) |
|---------------:|-----------------------:|--------------------:|
|           1024 |                  23.27 |               12.27 |
|           4096 |                   6.37 |               31.39 |
|          65536 |                  21.91 |               77.31 |
|        1048576 |                  25.01 |               76.67 |
|       16777216 |                  39.82 |               63.51 |
